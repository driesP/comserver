#include "tcpconnection.h"

tcpConnection::tcpConnection(QObject *parent) : QObject(parent)
{
    QString tempRoomName = "main";
    _room = new room(this, tempRoomName);
    server = new QTcpServer(this);
    connect(server, SIGNAL(newConnection()), this, SLOT(newConnection()));
    if(!server->listen(QHostAddress::Any, 8000))
    {
        __debug << "Server Could Not Be Started Due To:";
        __debug << server->errorString();
        return;
    }
    QList<QHostAddress> ipAddressList = QNetworkInterface::allAddresses();
    for(int i = 0; i < ipAddressList.size(); ++i)
    {
        if(ipAddressList.at(i) != QHostAddress::LocalHost && ipAddressList.at(i).toIPv4Address())
        {
            ipAddress = ipAddressList.at(i).toString();
            break;
        }
    }
    if(ipAddress.isEmpty())
    {
        ipAddress = QHostAddress(QHostAddress::LocalHost).toString();
    }
    __debug << "server running on " << ipAddress << ":" << server->serverPort();

}



void tcpConnection::newConnection()
{
    QTcpSocket *tempSock = server->nextPendingConnection();
    int tempId = _room->clients();
    Client *tempClient = new Client(tempSock, tempId);
    connect(tempClient, SIGNAL(dataReady(QString, int)), this, SLOT(newData(QString, int)));
    _room->addClient(tempClient);

}

void tcpConnection::newData(QString data, int ID)
{
    __debug << data;
    int tempId = _room->clients();
    for(int i = 0; i < tempId; i++)
    {
        _room->getClient(i)->sendData(data);
    }
}
