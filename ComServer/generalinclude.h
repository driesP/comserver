#ifndef GENERALINCLUDE
#define GENERALINCLUDE

// Include QLibs here


#include <QDebug>


// Include Custom Libs here

// General Defines

#define __debug_line qDebug() << __FILE__ << __LINE__
#define __debug qDebug()

#endif // GENERALINCLUDE

