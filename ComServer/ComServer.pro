QT += core network
QT -= gui

TARGET = ComServer
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    tcpconnection.cpp \
    room.cpp \
    client.cpp

HEADERS += \
    generalinclude.h \
    tcpconnection.h \
    main.h \
    room.h \
    client.h

