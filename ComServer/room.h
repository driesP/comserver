#ifndef ROOM_H
#define ROOM_H

#include <QList>
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QNetworkInterface>
#include <QRegExp>

#include "generalinclude.h"
#include "client.h"
class room
{
public:
    room(QObject *parent = 0, QString naam= "default");
    ~room();
    void setName(QString naam);
    QString name();
    void setClient(Client * c, int ID);
    Client * getClient(int ID);
    void addClient(Client *c);
    int clients();

private:
    QString _roomName;
    QList <Client*> _clients;
};

#endif // ROOM_H
