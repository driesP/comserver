#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QNetworkInterface>
#include <QRegExp>

#include "generalinclude.h"

class Client : public QObject
{
    Q_OBJECT
public:
    explicit Client(QTcpSocket *socket, int ID);
    virtual ~Client();
    void setName(QString name);
    QString name();
    void sendData(QString data);

signals:
    dataReady(QString data, int ID);
private slots:
    void onReadData();
private:
    QTcpSocket *_socket;
    QString _ClientName;
    int _id;
};

#endif // CLIENT_H
