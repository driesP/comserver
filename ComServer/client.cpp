#include "client.h"

Client::Client(QTcpSocket *socket, int ID) : _socket(socket)
{
    _id = ID;
    connect(socket, SIGNAL(readyRead()), this, SLOT(onReadData()));
}
Client::~Client()
{
    __debug << "Client destructed with name: " << _ClientName;
}

void Client::onReadData()
{
    unsigned long long bytesAvailable = _socket->bytesAvailable();
    char buf[bytesAvailable];
    _socket->read(buf,bytesAvailable);
    QString dataRecv;
    for (int i = 0; i < bytesAvailable; i++)
    {
        dataRecv.append(buf[i]);
    }
    emit dataReady(dataRecv, _id);
}

void Client::setName(QString name)
{
    _ClientName = name;
}
QString Client::name()
{
    return _ClientName;
}
void Client::sendData(QString data)
{
    QByteArray temp = QByteArray(data.toStdString().c_str());
    _socket->write(temp);
    _socket->flush();
}
