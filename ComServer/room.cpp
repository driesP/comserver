#include "room.h"

room::room(QObject *parent, QString naam)
{
    _roomName = naam;
    __debug << "Room Constructed with name: " << naam;

}
room::~room()
{
    __debug << "Room Destructed";
}
void room::setName(QString naam)
{
    _roomName = naam;
}
QString room::name()
{
    return _roomName;
}
void room::setClient(Client * c,int ID)
{
    _clients[ID] = c;
}
Client * room::getClient(int ID)
{
    return _clients[ID];
}
void room::addClient(Client * c)
{
    _clients.append(c);
}
int room::clients()
{
    return _clients.length();
}
