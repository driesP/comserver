#ifndef TCPCONNECTION_H
#define TCPCONNECTION_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QNetworkInterface>
#include <QRegExp>

#include "generalinclude.h"
#include "room.h"
#include "client.h"
class tcpConnection : public QObject
{
    Q_OBJECT

public:
    explicit tcpConnection(QObject *parent = 0);

public slots:
        void newConnection();
        void newData(QString data, int ID);
private:
    QTcpServer *server;
    QString ipAddress;
    QList <Client* > _clientList;
    room *_room;

};

#endif // TCPCONNECTION_H
